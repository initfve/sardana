Sardana config
==============

This directory contains a set of scripts to handle sardana configurations "offline", i.e. without requiring a running sardana instance. The configuration is described in a YAML text format.

The idea is to work directly upon the Tango database, using dsconfig. The scripts here don't interface directly with tango, but through reading and writing dsconfig JSON files.

While these scripts are directly runnable, the more ergonomic way to use them is via the common `sardana` wrapper script, via the `sardana config` command which provides slightly higher level functionality. See documentation of that tool for documentation.

To run these scripts directly, e.g.:

    python sardana.config.dsconfig2yaml my_dump.json macroserver/test/1
    
In order to be ergonomic when it comes to comparing versions etc, the scripts are written to enable "roundtripping" as far as reasonably possible. This means that converting e.g. from dsconfig -> yaml -> dsconfig again should result in a dsconfig file that is identical or at least very similar to the original. The idea is to make actual changes easy to detect. Perfect roundtripping may not always be possible, and can be somewhat tricky due to Tango's handling of properties as strings, and case insensitivity, among other things. There are also likely various bugs in this department. 

Note that the conversion does not persist things like key ordering, and YAML comments, but only "logical" content. Take a look at the `merge` script for a solution to this.


dsconfig2yaml
-------------

This script converts a dsconfig JSON dump into a sardana YAML config. The dump needs to contain the macroserver and pool(s) you are interested in, and as a second argument it expects the device name of the macroserver.

The resulting YAML is written to stdout. It's also possible to leave out the dump argument, the script will then read JSON from stdin.

Example:

    python -m dsconfig.dump server:MacroServer/test server:Pool/test | python -m sardana.config.dsconfig2yaml macroserver/test/1 > test.yaml

yaml2dsconfig
-------------

Not surprisingly, this script does the opposite of the above one. It just takes a YAML config as filename or from stdin, and prints out the resulting JSON to stdout

Example:
    
    python -m sardana.config.yaml2dsconfig test.yaml > test.json

    
validate
--------

Reads a YAML config and checks that it is properly formatted. This is a syntactic check that does not do "logic" checking of references, etc. For that, the `check` script exists.

If the format is found to be wrong, the script fails and some errors should be displayed to help fixing the problems.

    python -m sardana.config.validate test1.yaml

check
-----

Like the `validate` script, checks a YAML config for errors. It assumes that the format is correct (that `validate` would pass) and does further checking of internal references, as well as verifying that controller code exists, etc. This presupposes that the script runs in the same (or similar) installation as where the config is to be used, so that controllers can be found, etc.

If the format is found to be wrong, the script fails and some errors should be displayed to help fixing the problems.


    python -m sardana.config.check test1.yaml


diff
----

This script reads two YAML configurations and produces a list of differences from the first file to the second. This can be useful when looking for changes between e.g. two snapshots taken at different times. The output is intended to be somewhat human readable.

Example:

    python -m sardana.config.diff test1.yaml test2.yaml
    
    Pool_test_1
    - MOVED /pools/Pool_test_1/controllers/ctctrl17/elements/ct57 to /pools/Pool_test_1/controllers/ctctrl17/elements/ct55
    - REMOVED /pools/Pool_test_1/controllers/twodctrl15


merge
-----

(Note: this is a complex feature, we'll see how well it works in practice.)

One benefit of the YAML format is that it allows inline comments. However since we cannot store these in the dsconfig format, they will be lost in the conversion. Also, the ordering of keys in the file will not persist when converting to dsconfig and back.

As a solution to that problem, this script allows you to "merge" changes into an existing YAML config. The idea is that a YAML file that contains ordering and comments can still it with the current state of the installation without losing all the information. 

Let's say you have an `original.yaml` where you have organized things nice and commented. Now create an `update.yaml` from a dump of the same Sardana installation (see `dsconfig2yaml` above) and then run the script like this:

    python -m sardana.config.merge original.yaml update.yaml > new.yaml
    
Now `new.yaml` should contain the updated config, keeping your comments and ordering from the original (as far as possible).

Another point is that this makes the YAML diffs more readable if you intend to put the config in e.g. `git`. However, if you don't care about comments and ordering, it is probably better to rely on the `diff` script above.
